/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { StringBuilder } from "./HOSStringBuilder";
import { Log } from "../Log"
import { Utf8ArrayToStr } from "./Utf";
import { VCardConfig } from "../VCardConfig";
import { VCardConstants } from "../VCardConstants";
import { TextUtils } from "./TextUtils";
import { PhoneNumberUtils } from "./PhoneNumberUtils";
import { ContainerUtils } from "./ContainerUtils";
import { JapaneseUtils } from "../JapaneseUtils";
import {contact} from "../PlatformInterface/Contact";
import  {VCardParserImpl_V21}  from "../VCardParserImpl_V21";
import  {VCardParserImpl_V30}  from "../VCardParserImpl_V30";
import  {VCardParserImpl_V40} from "../VCardParserImpl_V40";
import {GlobalContext} from '../GlobalContext'
export namespace VCardUtils {
    let LOG_TAG = "VcardUtils";

    export class QuotedPrintableCodecPort {
        private static ESCAPE_CHAR: number = '='.charCodeAt(0);
        public static decodeQuotedPrintable(bytes: Uint8Array|null): Uint8Array | null {
            let bufferIndex = 0;
            if (bytes == null) {
                return null;
            }
            let buffer = new Uint8Array(bytes.length)
            for (let i = 0; i < bytes.length; i++) {
                //console.log(`i = ${i}`)
                let b: number = bytes[i];
                if (b == QuotedPrintableCodecPort.ESCAPE_CHAR) {
                    try {
                        let u: number = GlobalContext.parseInt(String.fromCharCode(bytes[++i]).toString(), 16);
                        let l: number = GlobalContext.parseInt(String.fromCharCode(bytes[++i]).toString(), 16);
                        //console.log(`u:${u}   l:${l}`)
                        if (u == -1 || l == -1) {
                            throw new Error("Invalid quoted-printable encoding");
                        }
                        let result: number = ((u << 4) + l) & 0x000000ff
                        Log.w("VcardUtils","decodeQuotedPrintable : "+result)
                        buffer[bufferIndex++] = result
                    } catch (e) {
                        throw new Error("Invalid quoted-printable encoding  --> " + e);
                    }
                } else {
                    buffer[bufferIndex++] = b
                    Log.w("VcardUtils","decodeQuotedPrintable : "+b)
                }
            }
            return buffer.slice(0, bufferIndex);
        }
    }

    export class QuotedPrintableCodecPortV2 {
        private static ESCAPE_CHAR: number = '='.charCodeAt(0);
        public static decodeQuotedPrintable(bytes: Uint8Array): Uint8Array | null {
            let bufferIndex = 0;
            if (bytes == null) {
                return null;
            }
            let buffer = new Uint8Array(512)
            for (let i = 0; i < bytes.length; i++) {
                let b: number = bytes[i];
                if (b == QuotedPrintableCodecPortV2.ESCAPE_CHAR) {
                    try {
                        let u: number = GlobalContext.parseInt(String.fromCharCode(bytes[++i]).toString(), 16);
                        let l: number = GlobalContext.parseInt(String.fromCharCode(bytes[++i]).toString(), 16);
                        if (u == -1 || l == -1) {
                            throw new Error("Invalid quoted-printable encoding");
                        }
                        let result: number = ((u << 4) + l) & 0x000000ff
                        buffer[bufferIndex++] = result
                    } catch (e) {
                        throw new Error("Invalid quoted-printable encoding  --> " + e);
                    }
                } else {
                    buffer[bufferIndex++] = b
                }
            }
            return buffer.slice(0, bufferIndex);
        }
    }

    /**
    * Ported methods which are hidden in {@link TextUtils}.
    */
    export class TextUtilsPort {
        public static isPrintableAscii(c: number): boolean {
            const asciiFirst: number = 0x20;
            const asciiLast: number = 0x7E;  // included
            return (asciiFirst <= c && c <= asciiLast) || c == '\r'.charCodeAt(0) || c == '\n'.charCodeAt(0);
        }

        public static isPrintableAsciiOnly(str: string): boolean {
            const len: number = str.length;
            for (let i = 0; i < len; i++) {
                if (!TextUtilsPort.isPrintableAscii(str.charCodeAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Checks to see if a string looks like it could be an system generated quoted printable.
     *
     * Identification of quoted printable is not 100% reliable since it's just ascii.  But given
     * the high number and exact location of generated = signs, there is a high likely-hood that
     * it would be.
     *
     * @return True if it appears like quoted printable.  False otherwise.
     */
    export function appearsLikeVCardQuotedPrintable(value: string): boolean {
        // Quoted printable is always in multiple of 3s. With optional 1 '=' at end.
        const remainder: number = (value.length % 3);
        if (value.length < 2 || (remainder != 1 && remainder != 0)) {
            return false;
        }
        for (let i = 0; i < value.length; i += 3) {
            if (value.charAt(i) != '=') {
                return false;
            }
        }
        return true;
    }


    /**
     * Unquotes given Quoted-Printable value. value must not be null.
     */
    export function parseQuotedPrintable(
        value: string, strictLineBreaking: boolean,
        sourceCharset: string, targetCharset: string): string {
        // "= " -> " ", "=\t" -> "\t".
        // Previous code had done this replacement. Keep on the safe side.
        let quotedPrintable: string;
        {
            const builder: StringBuilder = new StringBuilder();
            const length: number = value.length;
            for (let i = 0; i < length; i++) {
                let ch: string = value.charAt(i);
                if (ch == '=' && i < length - 1) {
                    let nextCh: string = value.charAt(i + 1);
                    if (nextCh == ' ' || nextCh == '\t') {
                        builder.append(nextCh);
                        i++;
                        continue;
                    }
                }
                builder.append(ch);
            }
            quotedPrintable = builder.toString();
        }

        let lines: Array<string>;
        if (strictLineBreaking) {
            lines = quotedPrintable.split("\r\n");
        } else {
            let builder: StringBuilder = new StringBuilder();
            let length: number = quotedPrintable.length;
            let list = new Array<string>();
            for (let i = 0; i < length; i++) {
                let ch = quotedPrintable.charAt(i);
                if (ch == '\n') {
                    list.push(builder.toString());
                    builder = new StringBuilder();
                } else if (ch == '\r') {
                    list.push(builder.toString());
                    builder = new StringBuilder();
                    if (i < length - 1) {
                        let nextCh = quotedPrintable.charAt(i + 1);
                        if (nextCh == '\n') {
                            i++;
                        }
                    }
                } else {
                    builder.append(ch);
                }
            }
            const lastLine: string = builder.toString();
            if (lastLine.length > 0) {
                list.push(lastLine);
            }
            lines = list
        }

        let builder = new StringBuilder();

        lines.forEach((line, index, array) => {
            if (line.endsWith("=")) {
                line = line.substring(0, line.length - 1);
            }
            builder.append(line);

        })

        let rawString: string = builder.toString();
        if (!rawString) {
            Log.w(LOG_TAG, "Given raw string is empty.");
        }

        let rawBytes: Uint8Array | null = null;
        try {
            let sb = new StringBuilder(sourceCharset)
            sb.append(rawString)
            Log.w("VCardUtils","rawString-->"+rawString)

            rawBytes = sb.getUint8Bytes();

            Log.w("VCardUtils","utf8Bytes-->"+rawBytes)
        } catch (e) {
            Log.w(LOG_TAG, "Failed to decode: " + sourceCharset+" --> e:"+e);
            let sb = new StringBuilder()
            sb.append(rawString)
            rawBytes = sb.getUint8Bytes();
        }


        let decodedBytes: Uint8Array | null = null;
        try {
            decodedBytes = QuotedPrintableCodecPort.decodeQuotedPrintable(rawBytes);
        } catch (e) {
            decodedBytes = rawBytes;
        }

        try {
            return Utf8ArrayToStr(decodedBytes!, targetCharset);

        } catch (e) {
            return Utf8ArrayToStr(decodedBytes!);
        }
    }

    /**
     * Splits the given value into pieces using the delimiter ';' inside it.
     *
     * Escaped characters in those values are automatically unescaped into original form.
     */
    export function constructListFromValue(value: string,
        vcardType: number): Array<string> {
        let list = new Array<string>();
        let builder = new StringBuilder();
        let length = value.length;
        for (let i = 0; i < length; i++) {
            let ch = value.charAt(i);
            if (ch == '\\' && i < length - 1) {
                let nextCh = value.charCodeAt(i + 1);
                let unescapedString: string | null = String.fromCharCode(nextCh);
                if (VCardConfig.isVersion40(vcardType)) {
                    unescapedString = VCardParserImpl_V40.unescapeCharacter(nextCh);
                } else if (VCardConfig.isVersion30(vcardType)) {
                    unescapedString = VCardParserImpl_V30.unescapeCharacter(nextCh);
                } else {
                    if (!VCardConfig.isVersion21(vcardType)) {
                        // Unknown vCard type
                        Log.w(LOG_TAG, "Unknown vCard type");
                    }
                    unescapedString = VCardParserImpl_V21.unescapeCharacter(nextCh);
                }
                if (unescapedString != null) {
                    builder.append(unescapedString);
                    i++;
                } else {
                    builder.append(ch);
                }
            } else if (ch == ';') {
                list.push(builder.toString());
                builder = new StringBuilder();
            } else {
                builder.append(ch);
            }
        }
        list.push(builder.toString());
        return list;
    }

    export function convertStringCharset(originalString: string, sourceCharset: string, targetCharset: string): string {
        if (sourceCharset.toLowerCase() == (targetCharset.toLowerCase())) {
            return originalString;
        }
        let sb = new StringBuilder(sourceCharset)
        sb.append(originalString)
        let bytesArray = sb.getUint8Bytes()
        return Utf8ArrayToStr(bytesArray, targetCharset)
    }

    const sKnownPhoneTypeMap_StoI: Map<string, number> = new Map([
        [VCardConstants.PARAM_TYPE_CAR, contact.PhoneNumber.NUM_CAR],
        [VCardConstants.PARAM_TYPE_PAGER, contact.PhoneNumber.NUM_PAGER],
        [VCardConstants.PARAM_TYPE_ISDN, contact.PhoneNumber.NUM_ISDN],
        [VCardConstants.PARAM_TYPE_HOME, contact.PhoneNumber.NUM_HOME],
        [VCardConstants.PARAM_TYPE_WORK, contact.PhoneNumber.NUM_WORK],
        [VCardConstants.PARAM_TYPE_CELL, contact.PhoneNumber.NUM_MOBILE],
        [VCardConstants.PARAM_PHONE_EXTRA_TYPE_OTHER, contact.PhoneNumber.NUM_OTHER],
        [VCardConstants.PARAM_PHONE_EXTRA_TYPE_CALLBACK, contact.PhoneNumber.NUM_CALLBACK],
        [VCardConstants.PARAM_PHONE_EXTRA_TYPE_COMPANY_MAIN, contact.PhoneNumber.NUM_COMPANY_MAIN],
        [VCardConstants.PARAM_PHONE_EXTRA_TYPE_RADIO, contact.PhoneNumber.NUM_RADIO],
        [VCardConstants.PARAM_PHONE_EXTRA_TYPE_TTY_TDD, contact.PhoneNumber.NUM_TTY_TDD],
        [VCardConstants.PARAM_PHONE_EXTRA_TYPE_ASSISTANT, contact.PhoneNumber.NUM_ASSISTANT],
        [VCardConstants.PARAM_TYPE_VOICE, contact.PhoneNumber.NUM_OTHER]
    ]);

    const sKnownPhoneTypesMap_ItoS: Map<number, string> = new Map(
        [
            [contact.PhoneNumber.NUM_CAR, VCardConstants.PARAM_TYPE_CAR],
            [contact.PhoneNumber.NUM_PAGER, VCardConstants.PARAM_TYPE_PAGER],
            [contact.PhoneNumber.NUM_ISDN, VCardConstants.PARAM_TYPE_ISDN]
        ]
    );
    const sPhoneTypesUnknownToContactsSet: Set<string> = new Set<string>(
        [
            VCardConstants.PARAM_TYPE_MODEM,
            VCardConstants.PARAM_TYPE_MSG,
            VCardConstants.PARAM_TYPE_BBS,
            VCardConstants.PARAM_TYPE_VIDEO,
        ]
    );
    const sKnownImPropNameMap_ItoS: Map<number, string> = new Map([
        [contact.ImAddress.IM_AIM, VCardConstants.PROPERTY_X_AIM],
        [contact.ImAddress.IM_MSN, VCardConstants.PROPERTY_X_MSN],
        [contact.ImAddress.IM_YAHOO, VCardConstants.PROPERTY_X_YAHOO],
        [contact.ImAddress.IM_SKYPE, VCardConstants.PROPERTY_X_SKYPE_USERNAME],
        [contact.ImAddress.IM_QQ, VCardConstants.PROPERTY_X_QQ],
        [contact.ImAddress.IM_ICQ, VCardConstants.PROPERTY_X_ICQ],
        [contact.ImAddress.IM_JABBER, VCardConstants.PROPERTY_X_JABBER]
    ]);
    const sMobilePhoneLabelSet: Set<string> = new Set<string>([
        "MOBILE",
        "\u643A\u5E2F\u96FB\u8A71",
        "\u643A\u5E2F",
        "\u30B1\u30A4\u30BF\u30A4",
        "\uFF79\uFF72\uFF80\uFF72"
    ]);

    /**
     * Returns Integer when the given types can be parsed as known type. Returns String object
     * when not, which should be set to label.
     */
    export function getPhoneTypeFromStrings(types: string[] | Set<string> | undefined |ESObject,
        number: string | null): number | string {

        if (number == null) {
            number = "";
        }
        let type = -1;
        let label: string | null
        let isFax = false;
        let hasPref = false;

        if (types != null) {
            for (let i = 0;i<types.length;i++) {
                if (types[i] == null) {
                    continue;
                }
                const typeStringUpperCase:ESObject = types[i].toUpperCase();
                if (typeStringUpperCase == (VCardConstants.PARAM_TYPE_PREF)) {
                    hasPref = true;
                } else if (typeStringUpperCase == (VCardConstants.PARAM_TYPE_FAX)) {
                    isFax = true;
                } else {
                    let labelCandidate:ESObject;
                    if (typeStringUpperCase.startsWith("X-") && type < 0) {
                        labelCandidate = types[i].substring(2);
                    } else {
                        labelCandidate = types[i];
                    }
                    if (labelCandidate.length == 0) {
                        continue;
                    }
                    // e.g. "home" -> TYPE_HOME
                    let tmp: number | undefined = sKnownPhoneTypeMap_StoI.get(labelCandidate.toUpperCase());
                    if (tmp != null) {
                        const typeCandidate = tmp;
                        // 1. If a type isn't specified yet, we'll choose the new type candidate.
                        // 2. If the current type is default one (OTHER) or custom one, we'll
                        // prefer more specific types specified in the vCard. Note that OTHER and
                        // the other different types may appear simultaneously here, since vCard
                        // allow to have VOICE and HOME/WORK in one line.
                        // e.g. "TEL;WORK;VOICE:1" -> WORK + OTHER -> Type should be WORK
                        // 3. TYPE_PAGER is preferred when the number contains @ surrounded by
                        // a pager number and a domain name.
                        // e.g.
                        // o 1111@domain.com
                        // x @domain.com
                        // x 1111@
                        const indexOfAt = number.indexOf("@");
                        if ((typeCandidate == contact.PhoneNumber.NUM_PAGER
                            && 0 < indexOfAt && indexOfAt < number.length - 1)
                            || type < 0
                            || type == contact.PhoneNumber.CUSTOM_LABEL
                            || type == contact.PhoneNumber.NUM_OTHER) {
                            type = tmp;
                        }
                    } else if (type < 0) {
                        type = contact.PhoneNumber.CUSTOM_LABEL;
                        label = labelCandidate;
                    }
                }
            }
        }
        if (type < 0) {
            if (hasPref) {
                type = contact.PhoneNumber.NUM_MAIN;
            } else {
                // default to TYPE_HOME
                type = contact.PhoneNumber.NUM_HOME;
            }
        }
        if (isFax) {
            if (type == contact.PhoneNumber.NUM_HOME) {
                type = contact.PhoneNumber.NUM_FAX_HOME;
            } else if (type == contact.PhoneNumber.NUM_WORK) {
                type = contact.PhoneNumber.NUM_FAX_WORK;
            } else if (type == contact.PhoneNumber.NUM_OTHER) {
                type = contact.PhoneNumber.NUM_OTHER_FAX;
            }
        }
        if (type == contact.PhoneNumber.CUSTOM_LABEL) {
            return label!;
        } else {
            return type;
        }
    }

    export function constructNameFromElements(nameOrder: number,
        familyName: string, middleName: string, givenName: string,
        prefix?: string, suffix?: string): string {
        const builder = new StringBuilder();
        const nameList: string[] = sortNameElements(nameOrder, familyName, middleName, givenName);
        let first = true;
        if (!TextUtils.isEmpty(prefix)) {
            first = false;
            builder.append(prefix!);
        }
        for (let namePart of nameList) {
            if (!TextUtils.isEmpty(namePart)) {
                if (first) {
                    first = false;
                } else {
                    builder.append(' ');
                }
                builder.append(namePart);
            }
        }
        if (!TextUtils.isEmpty(suffix)) {
            if (!first) {
                builder.append(' ');
            }
            builder.append(suffix!);
        }
        return builder.toString();
    }

    export function sortNameElements(nameOrder: number,
        familyName: string, middleName: string, givenName: string): string[] {
        let list: Array<string> = new Array(3);
        let nameOrderType: number = VCardConfig.getNameOrderType(nameOrder);
        switch (nameOrderType) {
            case VCardConfig.NAME_ORDER_JAPANESE: {
                if (containsOnlyPrintableAscii(familyName) &&
                    containsOnlyPrintableAscii(givenName)) {
                    list[0] = givenName;
                    list[1] = middleName;
                    list[2] = familyName;
                } else {
                    list[0] = familyName;
                    list[1] = middleName;
                    list[2] = givenName;
                }
                break;
            }
            case VCardConfig.NAME_ORDER_EUROPE: {
                list[0] = middleName;
                list[1] = givenName;
                list[2] = familyName;
                break;
            }
            default: {
                list[0] = givenName;
                list[1] = middleName;
                list[2] = familyName;
                break;
            }
        }
        return list;
    }

    export function containsOnlyPrintableAscii(...values: string[]): boolean
    export function containsOnlyPrintableAscii(values: string[]): boolean
    export function containsOnlyPrintableAscii(values: ESObject): boolean {
        if (values == null) {
            return true;
        }
        for (let index = 0;index<values.length;index++) {
            if (TextUtils.isEmpty(values[index])) {
                continue;
            }
            if (!TextUtilsPort.isPrintableAsciiOnly(values[index])) {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * This is useful when checking the string should be encoded into quoted-printable
     * or not, which is required by vCard 2.1.
     * </p>
     * <p>
     * See the definition of "7bit" in vCard 2.1 spec for more information.
     * </p>
     */
    /*
    public static boolean containsOnlyNonCrLfPrintableAscii(final String...values) {
        if (values == null) {
            return true;
        }
        return containsOnlyNonCrLfPrintableAscii(Arrays.asList(values));
    }
    */

    export function containsOnlyNonCrLfPrintableAscii(...values: string[]): boolean;
    export function containsOnlyNonCrLfPrintableAscii(values: Array<string> | null): boolean;
    export function containsOnlyNonCrLfPrintableAscii(values: ESObject): boolean {
        if (values == null) {
            return true;
        }
        const asciiFirst = 0x20;
        const asciiLast = 0x7E;  // included
        for (let index = 0;index<values.length;index++) {
            if (TextUtils.isEmpty(values[index])) {
                continue;
            }
            const length:number = values[index].length;
            for (let i = 0; i < length; i = TextUtils.offsetByCodePoints(values[index], i, 1)) {
                const c:ESObject = values[index].codePointAt(i);
                if (!(asciiFirst <= c! && c! <= asciiLast)) {
                    return false;
                }
            }
        }
        return true;
    }

    const sUnAcceptableAsciiInV21WordSet: Set<string> =
        new Set<string>(['[', ']', '=', ':', '.', ',', ' ']);

    export function containsOnlyAlphaDigitHyphen(...values: string[]): boolean

    export function containsOnlyAlphaDigitHyphen(values:ESObject): boolean {
        if (values == null) {
            return true;
        }
        const upperAlphabetFirst = 0x41;  // A
        const upperAlphabetAfterLast = 0x5b;  // [
        const lowerAlphabetFirst = 0x61;  // a
        const lowerAlphabetAfterLast = 0x7b;  // {
        const digitFirst = 0x30;  // 0
        const digitAfterLast = 0x3A;  // :
        const hyphen = '-';
        for (let index = 0;index<values.length;index++) {
            if (TextUtils.isEmpty(values[index])) {
                continue;
            }
            let length:number = values[index].length;
            for (let i = 0; i < length; i = TextUtils.offsetByCodePoints(values[index],i,1)) {
                const codepoint:ESObject = values[index].codePointAt(i);
                if (!((lowerAlphabetFirst <= codepoint && codepoint < lowerAlphabetAfterLast) ||
                    (upperAlphabetFirst <= codepoint && codepoint < upperAlphabetAfterLast) ||
                    (digitFirst <= codepoint && codepoint < digitAfterLast) ||
                    (codepoint == hyphen))) {
                    return false;
                }
            }
        }
        return true;
        
    }

    export function containsOnlyWhiteSpaces(...values: string[]): boolean

    export function containsOnlyWhiteSpaces(values:ESObject) {
        if (values == null) {
            return true;
        }
        for (let index = 0;index<values.length;index++) {
            if (TextUtils.isEmpty(values[index])) {
                continue;
            }
            const length:number = values[index].length;
            for (let i = 0; i < length; i = TextUtils.offsetByCodePoints(values[index], i, 1)) {
                if (!isWhitespace(values[index].codePointAt(i)!)) {
                    return false;
                }
            }
        }
        return true;
    }

    export function isWhitespace(ch: number): boolean {
        return (ch <= 0x0020) && (((((1 << 0x0009) | (1 << 0x000A) | (1 << 0x000C) | (1 << 0x000D) | (1 << 0x0020)) >> ch) & 1) != 0);
    }

    export function getFormattedPostalAddress(vcardType: number, c: contact.PostalAddress) {

        let builder = new StringBuilder();
        let empty = true;
        let dataArray:ESObject =
            [c.pobox, c.postalAddress, c.street, c.city, c.region, c.postcode, c.country]

        if (VCardConfig.isJapaneseDevice(vcardType)) {
            // In Japan, the order is reversed.
            for (let i = 7 - 1; i >= 0; i--) {
                let addressPart:ESObject = dataArray[i];
                if (!TextUtils.isEmpty(addressPart)) {
                    if (!empty) {
                        builder.append(' ');
                    } else {
                        empty = false;
                    }
                    builder.append(addressPart!);
                }
            }
        } else {
            for (let i = 0; i < 7; i++) {
                let addressPart:ESObject = dataArray[i];
                if (!TextUtils.isEmpty(addressPart)) {
                    if (!empty) {
                        builder.append(' ');
                    } else {
                        empty = false;
                    }
                    builder.append(addressPart!);
                }
            }
        }
        return builder.toString().trim();
    }

    export function getFormattedOrganizationString(organize: contact.Organization): string {
        const builder = new StringBuilder();
        if (!TextUtils.isEmpty(organize.name)) {
            builder.append(organize.name!);
        }

        if (!TextUtils.isEmpty(organize.title)) {
            if (builder.size() > 0) {
                builder.append(", ");
            }
            builder.append(organize.title!);
        }
        return builder.toString();
    }

    export function getPhoneNumberFormat(vcardType: number): number {
        if (VCardConfig.isJapaneseDevice(vcardType)) {
            return PhoneNumberUtils.FORMAT_JAPAN;
        } else {
            return PhoneNumberUtils.FORMAT_NANP;
        }
    }

    export function areAllEmpty(...values: string[]) {
        if (values == null) {
            return true
        }
        for (let value of values) {
            if (!TextUtils.isEmpty(value)) {
                return false;
            }
        }
        return true
    }

    export function getPropertyNameForIm(protocol: number) {
        return sKnownImPropNameMap_ItoS.get(protocol)
    }
    export function isMobilePhoneLabel(label: string): boolean {
        // For backward compatibility.
        // Detail: Until Donut, there isn't TYPE_MOBILE for email while there is now.
        //         To support mobile type at that time, this custom label had been used.
        return ("_AUTO_CELL" == (label) || ContainerUtils.contains(sMobilePhoneLabelSet, label));
    }

    let sEscapeIndicatorsV30 = [':'.charCodeAt(0), ';'.charCodeAt(0), ','.charCodeAt(0), ' '.charCodeAt(0)]
    let sEscapeIndicatorsV40 = [';'.charCodeAt(0), ':'.charCodeAt(0)]

    export function isValidInV21ButUnknownToContactsPhoteType(label: string) {
        return ContainerUtils.contains(sPhoneTypesUnknownToContactsSet, label);
    }

    export function getPhoneTypeString(type: number): string | undefined {
        return sKnownPhoneTypesMap_ItoS.get(type);
    }
    export function toStringAsV30ParamValue(value: string): string {
        return toStringAsParamValue(value, sEscapeIndicatorsV30);
    }

    export function toStringAsV40ParamValue(value: string) {
        return toStringAsParamValue(value, sEscapeIndicatorsV40);
    }

    export function toStringAsParamValue(value: string, escapeIndicators: Array<number>): string {
        if (TextUtils.isEmpty(value)) {
            value = "";
        }
        let asciiFirst = 0x20;
        let asciiLast = 0x7E;  // included
        let builder = new StringBuilder();
        let length = value.length;
        let needQuote = false;
        for (let i = 0; i < length; i = TextUtils.offsetByCodePoints(value, i, 1)) {
            let codePoint = value.codePointAt(i)!;
            if (codePoint < asciiFirst || codePoint == '"'.codePointAt(0)) {
                // CTL characters and DQUOTE are never accepted. Remove them.
                continue;
            }
            builder.append(String.fromCodePoint(codePoint))
            //builder.appendCodePoint(codePoint);
            for (let indicator of escapeIndicators) {
                if (codePoint == indicator) {
                    needQuote = true;
                    break;
                }
            }
        }
        let result = builder.toString();
        return ((result.length == 0 || VCardUtils.containsOnlyWhiteSpaces(result))
            ? ""
            : (needQuote ? ('"' + result + '"')
                : result));
    }

    export function toHalfWidthString(orgString: string|null): string | null {
        if (TextUtils.isEmpty(orgString)) {
            return null;
        }
        let builder = new StringBuilder();
        let length = orgString!.length;
        for (let i = 0; i < length; i = TextUtils.offsetByCodePoints(orgString!, i, 1)) {
            // All Japanese character is able to be expressed by char.
            // Do not need to use String#codepPointAt().
            let ch = orgString!.codePointAt(i);
            let halfWidthText = JapaneseUtils.tryGetHalfWidthText(String.fromCodePoint(ch!));
            if (halfWidthText != null) {
                builder.append(halfWidthText);
            } else {
                builder.append(String.fromCodePoint(ch!));
            }
        }
        return builder.toString();
    }

    /**
    * <p>
    * Returns true when the given String is categorized as "word" specified in vCard spec 2.1.
    * </p>
    * <p>
    * vCard 2.1 specifies:<br />
    * word = &lt;any printable 7bit us-ascii except []=:., &gt;
    * </p>
    */
    export function isV21Word(value: string): boolean {
        if (TextUtils.isEmpty(value)) {
            return true;
        }
        let asciiFirst = 0x20;
        let asciiLast = 0x7E;  // included
        let length = value.length;
        for (let i = 0; i < length; i = TextUtils.offsetByCodePoints(value, i, 1)) {
            let c = value.codePointAt(i)!;
            if (!(asciiFirst <= c && c <= asciiLast) ||
                sUnAcceptableAsciiInV21WordSet.has(String.fromCodePoint(c))) {
                return false;
            }
        }
        return true;
    }
}



